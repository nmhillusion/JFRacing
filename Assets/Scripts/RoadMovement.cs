﻿using UnityEngine;

public class RoadMovement : MonoBehaviour, SpeedIncreasable
{
    //  USER SETTING
    public AudioClip runningClip;

    private AudioSource audioSource;

    private GameObject myObj;
    public float rangeAvailable = 4.0f;
    private Vector3 lastPosition;
    private float speed = 4.0f;

    void Start()
    {
        myObj = gameObject;
        lastPosition = myObj.transform.position;
        GameController.AddSpeedIncreasable(this);


        audioSource = myObj.GetComponent<AudioSource>();
        audioSource.volume = 0.1f;
        audioSource.clip = runningClip;
        audioSource.loop = true;
    }

    void Update()
    {
        if (Vector3.Distance(lastPosition, myObj.transform.position) >= rangeAvailable)
        {
            myObj.transform.position = lastPosition;
        }
        else
        {
            myObj.transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
        }

        if (Time.timeScale == 0)
        {
            audioSource.Stop();
        }else if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    public void SpeedUp(float amount)
    {
        speed += amount;
        audioSource.volume += 0.1f;
    }
}

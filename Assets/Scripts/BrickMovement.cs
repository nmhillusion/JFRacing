﻿using UnityEngine;

public class BrickMovement : MonoBehaviour, SpeedIncreasable
{

    private float speed = 4.0f;

    private GameObject myObj;
    /**
     * @lastBrickToContinue is the highest brick to this brick calculate position of reset y position
     **/
    private static GameObject lastBrickToContinue = null;

    /**
     * standard X Position is the position where brick is in the middle of the road
     * */
    private const float standardXPosition = 8.4f;
    private static float targetXPosition = standardXPosition,
                            currentXPosition = targetXPosition;
    private const float HEIGHT_OF_BRICK = 0.9f;

    void Start()
    {
        myObj = gameObject;
        GameController.AddSpeedIncreasable(this);
    }

    void Update()
    {
        myObj.transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("resetBrick"))
        {
            if (Mathf.Abs(currentXPosition - targetXPosition) <= 0.1f)
            {
                targetXPosition = RandomBrickXAxis();
            }

            if (null == lastBrickToContinue || !lastBrickToContinue.name.Equals(myObj.name))
            {
                currentXPosition = GetXToTargetPosition();
                myObj.transform.position = new Vector3(currentXPosition, GetInitYPosition(), myObj.transform.position.z);
                lastBrickToContinue = myObj;
            }
        }
    }

    private float GetInitYPosition()
    {
        if (lastBrickToContinue != null)
        {
            return lastBrickToContinue.transform.position.y + HEIGHT_OF_BRICK;
        }
        else
        {
            return -8.0f; //    as design UI of Unity for highest brick
        }
    }

    private float GetXToTargetPosition()
    {
        float deltaSpace = Random.Range(0, 3) < 1.0f ? Random.Range(-0.01f, 0.2f) : Random.Range(-0.01f, 0.06f);
        if (currentXPosition < targetXPosition)
        {
            return currentXPosition + deltaSpace;
        }
        else
        {
            return currentXPosition - deltaSpace;
        }
    }

    private float RandomBrickXAxis()
    {
        return Random.Range(-1.2f, 1.2f) + standardXPosition;
    }

    public void SpeedUp(float amount)
    {
        speed += amount;
        Debug.Log("newSpeed: " + speed);
    }
}
﻿using UnityEngine;

public class CarController : MonoBehaviour
{
    enum NAVIGATION{
        LEFT,
        AHEAD,
        RIGHT
    };

    private NAVIGATION nav = NAVIGATION.AHEAD;
    private float CONST_Y_AXIS;

    // USER SETTING
    public GameController gameController;
    public float pushPower = 0.1f;
    public AudioClip collisionClip;
    public AudioClip crashedClip;

    private GameObject myObj;
    private AudioSource audioSource;
    private Animator animator;
    private const byte MAX_HEALTH = 20;
    private byte heart = MAX_HEALTH;

    void Start()
    {
        myObj = gameObject;
        CONST_Y_AXIS = myObj.transform.position.y;
        audioSource = myObj.GetComponent<AudioSource>();
        audioSource.clip = collisionClip;

        animator = myObj.GetComponent<Animator>();
        animator.SetBool("endGame", false);
    }

    void Update()
    {
        if (!gameController.IsEndGame())
        {
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                myObj.transform.Translate(new Vector3(-pushPower, CONST_Y_AXIS - myObj.transform.position.y, 0));
                TurnLeft();
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                myObj.transform.Translate(new Vector3(pushPower, CONST_Y_AXIS - myObj.transform.position.y, 0));
                TurnRight();
            }
        }
    }

    private void TurnLeft()
    {
        if(nav != NAVIGATION.LEFT)
        {
            myObj.transform.Rotate(0, 0, 5);

            if(nav == NAVIGATION.AHEAD)
            {
                nav = NAVIGATION.LEFT;
            }
            else
            {
                nav = NAVIGATION.AHEAD;
            }
        }
    }

    private void TurnRight()
    {
        if (nav != NAVIGATION.RIGHT)
        {
            myObj.transform.Rotate(0, 0, -5);

            if (nav == NAVIGATION.AHEAD)
            {
                nav = NAVIGATION.RIGHT;
            }
            else
            {
                nav = NAVIGATION.AHEAD;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        --heart;
        if (heart < 1)
        {
            EndGame();
        }
        else
        {
            myObj.transform.position = new Vector3(myObj.transform.position.x, myObj.transform.position.y, 0);
            gameController.BreakCar(heart*100/MAX_HEALTH);

            audioSource.Play();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals(GameController.TAG_SCORE))
        {
            gameController.IncreaseScore(other);
        }
    }

    private void EndGame()
    {
        gameController.GameOver();

        audioSource.clip = crashedClip;
        audioSource.Play();

        animator.SetBool("endGame", true);
    }
}

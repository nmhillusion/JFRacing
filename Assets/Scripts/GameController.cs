﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour {

    // USER SETTING
    public Text scoreText;
    public Slider healthSlider;

    private int score = 0;
    private bool endGame;
    private static readonly List<SpeedIncreasable> speedIncreasables = new List<SpeedIncreasable>();
    public static readonly string TAG_SCORE = "checkpoint-score";
    private const int MAX_SCORE_TO_SPEED_UP = 800;
    private AudioSource audioSource;

    void Start () {
        endGame = false;
        audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	void Update () {
		
	}

    public static void AddSpeedIncreasable(SpeedIncreasable speedIncreasable)
    {
        speedIncreasables.Add(speedIncreasable);
    }

    public void IncreaseScore(Collider2D checkPoint)
    {
        if (checkPoint.tag.Equals(TAG_SCORE))
        {
            ++score;
            scoreText.text = "score: " + score;

            if(score % 10 == 0 && score < MAX_SCORE_TO_SPEED_UP)
            {
                foreach(SpeedIncreasable s in speedIncreasables)
                {
                    s.SpeedUp(0.2f);
                }
            }
        }
    }

    public void BreakCar(float currentBlood)
    {
        if (0 <= currentBlood && currentBlood < 100)
        {
            healthSlider.value = currentBlood;
        }
    }

    internal bool IsEndGame()
    {
        return endGame;
    }

    public void GameOver()
    {
        endGame = true;
        Debug.Log("GAME OVER");
        Time.timeScale = 0;
        healthSlider.value = 0;

        audioSource.Stop();
    }
}
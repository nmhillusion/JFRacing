﻿public interface SpeedIncreasable {
    void SpeedUp(float amount);
}
